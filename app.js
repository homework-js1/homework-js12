//Чому для роботи з input не рекомендується використовувати клавіатуру?
//Це ненадійно, тому що введення даних не обов'язково може здійснюватися за допомогою клавіатури.
// Існують спеціальні події для обробки введення. Вони спрацьовують у результаті будь-якого введення,
// включаючи копіювати/вставити мишею та розпізнавання мови. Події клавіатури повинні використовуватися
// тільки за призначенням – для клавіатури.


const buttons = document.querySelectorAll(".btn");
document.addEventListener("keydown", handler);

function handler(e) {
    for (let i = 0; i < buttons.length; i++) {
        if ("Key" + buttons[i].textContent === e.code || buttons[i].textContent === e.key) {
            buttons[i].style.backgroundColor = "blue";
        } else (buttons[i].style.backgroundColor = "black")
    }
}
